<script src="http://code.jquery.com/jquery-1.4.2.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#btnAdd").click(function() {
            var num     = $(".clonedSection").length;
            var newNum  = new Number(num + 1);

            var newSection = $("#clonedSection" + num).clone().attr("id", "clonedSection" + newNum);

            newSection.children(":first").children(":first").attr("id", "name" + newNum).attr("name", "name" + newNum);
            newSection.children(":nth-child(2)").children(":first").attr("id", "brand" + newNum).attr("name", "brand" + newNum);
            newSection.children(":nth-child(3)").children(":first").attr("id", "country" + newNum).attr("name", "country" + newNum);

            $(".clonedSection").last().append(newSection)

            $("#btnDel").attr("disabled","");

            if (newNum == 5)
                $("#btnAdd").attr("disabled","disabled");
        });

        $("#btnDel").click(function() {
            var num = $(".clonedSection").length; // how many "duplicatable" input fields we currently have
            $("#clonedSection" + num).remove();     // remove the last element

            // enable the "add" button
            $("#btnAdd").attr("disabled","");

            // if only one element remains, disable the "remove" button
            if (num-1 == 1)
                $("#btnDel").attr("disabled","disabled");
        });

        $("#btnDel").attr("disabled","disabled");
    });
</script>

<form id="myForm">
    <div id="clonedSection1" class="clonedSection">
        <p>Name: <input type="text" name="name" id="name" /></p>
        <p>Brand Name: <input type="text" name="brand" id="brand" /></p>
        <p><select id="country" name="country">
          <option value="volvo">Volvo</option>
          <option value="saab">Saab</option>
          <option value="mercedes">Mercedes</option>
          <option value="audi">Audi</option>
        </select></p>
    </div>
    <div>
    <input type="submit">
        <input type="button" id="btnAdd" value="add another name" />
        <input type="button" id="btnDel" value="remove name" />
    </div>
</form>